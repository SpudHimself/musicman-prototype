﻿using UnityEngine;
using System.Collections;

public class audioReact : MonoBehaviour
{

    float[] samples;

    public GameObject movingObject;
	public AudioSource audioObject;
    float movingObjectScaleX;
    float movingObjectScaleY;
	float movingObjectOriginalX;
	float movingObjectOriginalY;
    
	void Start()
    {
        //make an array to contain all the samples of the clip currently playing
        //samples = new float[GetComponent<AudioSource>().clip.samples * GetComponent<AudioSource>().clip.channels];
		samples = new float[audioObject.clip.samples * audioObject.clip.channels];
        //store all the sample data of the clip in this array.
        //GetComponent<AudioSource>().clip.GetData(samples, 0);
		audioObject.clip.GetData (samples, 0);

        movingObjectOriginalX = movingObject.transform.localScale.x;
		movingObjectOriginalY = movingObject.transform.localScale.y;
		movingObjectScaleX = movingObject.transform.localScale.x;
        movingObjectScaleY = movingObject.transform.localScale.y;

    }
    void Update()
    {
		if (audioObject.mute == false) 
		{
			//in every frame get the current sample of the clip from the samples array.
        
			//float currentSample = samples[audio.timeSamples];
			//changed to
			//float currentSample = samples[GetComponent<AudioSource>().timeSamples * GetComponent<AudioSource>().clip.channels];
			float currentSample = samples [audioObject.timeSamples * audioObject.clip.channels];
			/*then do whatever you want with it,
        e.g. you can use it to set the Y position
        of a gameobject so that it moves up and down
        according to the beat. */

			movingObjectScaleX -= currentSample;
			movingObjectScaleY -= currentSample;

			//moves to the music
			movingObject.transform.localScale = new Vector3 (movingObjectScaleX, movingObjectScaleY, 0.0f);


			//trying to get it to shrink during music, visualizer style
			if ((movingObjectScaleX > 1) & (movingObjectScaleY > 1)) {
				movingObjectScaleX -= 0.1f;
				movingObjectScaleY -= 0.1f;
			}
			if ((movingObjectScaleX <= movingObjectOriginalX) & (movingObjectScaleY <= movingObjectOriginalY)) {
				movingObjectScaleX = movingObjectOriginalX;
				movingObjectScaleY = movingObjectOriginalY;
			}
		}
        
    }

}
