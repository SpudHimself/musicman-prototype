﻿using UnityEngine;
using System.Collections;

public class AudioMove : MonoBehaviour 
{
    float[] samples;

    public GameObject movingObject;
    public GameObject audioSource;
    //float movingObjectScaleX;
    //float movingObjectScaleY;
    float movingObjectPositionX;
    float movingObjectPositionY;

    void Start()
    {
        //make an array to contain all the samples of the clip currently playing
        samples = new float[GetComponent<AudioSource>().clip.samples * GetComponent<AudioSource>().clip.channels];
        //store all the sample data of the clip in this array.
        GetComponent<AudioSource>().clip.GetData(samples, 0);

        //movingObjectScaleX = movingObject.transform.localScale.x;
        //movingObjectScaleY = movingObject.transform.localScale.y;
        movingObjectPositionX = movingObject.transform.position.x;
        movingObjectPositionY = movingObject.transform.position.y;

    }

    void Update()
    {
        //in every frame get the current sample of the clip from the samples array.

        //float currentSample = samples[audio.timeSamples];
        //changed to
        float currentSample = samples[GetComponent<AudioSource>().timeSamples * GetComponent<AudioSource>().clip.channels];
        /*then do whatever you want with it,
        e.g. you can use it to set the Y position
        of a gameobject so that it moves up and down
        according to the beat. */

       // movingObjectScaleX -= currentSample;
       // movingObjectScaleY -= currentSample;

        //moves to the music
       // movingObject.transform.localScale = new Vector3(movingObjectScaleX, movingObjectScaleY, 0.0f);

        //movingObject.transform.position.x += currentSample;

		if (GetComponent<AudioSource>().volume != 0)

		movingObjectPositionY += currentSample;

		movingObject.transform.position = new Vector2 (movingObjectPositionX, movingObjectPositionY);
    }

}