﻿using UnityEngine;
using System.Collections;

public class ButtonQuitGame : MonoBehaviour 
{
	public void Exit()
	{
		Application.Quit();
	}
}
